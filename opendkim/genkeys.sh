#!/bin/sh

mkdir clefs
cd clefs
docker run --rm -v $(pwd):/tmp -w /tmp --entrypoint opendkim-genkey instrumentisto/opendkim --subdomains --domain=exemple.fr --selector=default
cd ..
